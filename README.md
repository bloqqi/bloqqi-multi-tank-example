This repository consists of a tank regulator and a tank model, both defined as Bloqqi programs, that are run in parallel and communicates with each other.

Run the example with ```make```:

    $ make
    java -jar tools/bloqqi-compiler.jar --c=configuration --o=gen/program.c \
        regulator.dia tank.dia configuration.dia
    gcc gen/program.c external.c
    ./a.out
    level: 0.000, setLevel: 0.4
    level: 0.018, setLevel: 0.4
    level: 0.038, setLevel: 0.4
    level: 0.058, setLevel: 0.4
    level: 0.078, setLevel: 0.4
    ...
