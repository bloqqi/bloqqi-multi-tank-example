external SquareRoot(in: Real => out: Real);

diagramtype Tank {
	input upperValve: Bool;
	input lowerValve: Bool;
	output level: Real;
	var currentLevel: Real;
	computeLevel: ComputeLevel;

	connect(currentLevel, computeLevel.currentLevel);
	connect(upperValve, computeLevel.upperValve);
	connect(lowerValve, computeLevel.lowerValve);

	connect(computeLevel, currentLevel);
	connect(computeLevel, level);
}

function ComputeLevel(
		currentLevel: Real,
		upperValve: Bool,
		lowerValve: Bool
		=> newLevel: Real) {
	Real flowLevel    = 0.005;   // m^3/s  (5 litre/s)
	Real area         = 0.25;    // 0.5*0.5 m^2
	Real outValveArea = 0.0025;  // 0.05*0.05 m^2
	Real hz           = 10;      // 10 hz

	newLevel = currentLevel;
	if (upperValve) {
		Real change = flowLevel/area;
		newLevel = newLevel + change/hz;
	}
	if (lowerValve) {
		Real change = (outValveArea*SquareRoot(2*9.82*currentLevel))/area;
		newLevel = newLevel - change/hz;
	}
}


/*
	// MODELICA MODEL

	// 1 litre = 1000 cm3
	parameter Real flowLevel(unit="cm3/s")  = 5000.0;  // 5 litre/s
	parameter Real area(unit="cm2")         = 2500.0;  // 50*50 cm2
	parameter Real outValveArea(unit="cm2") = 25.0;    // 5*5

	Real level(start=0, unit="cm");
	Real qInFlow(unit="cm3/s");
	Real qOutFlow(unit="cm3/s");

	input Real upperValveOpen(start=0.0);
	input Real lowerValveOpen(start=0.0);
equation
	der(level) = (qInFlow-qOutFlow)/area;
	qInFlow = if upperValveOpen > 0.0 then flowLevel else 0.0;
	qOutFlow = if lowerValveOpen > 0.0 then (outValveArea*sqrt(2*982*level)) else 0.0;
*/
